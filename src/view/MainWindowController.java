package view;

import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.*;

public class MainWindowController implements Initializable, View {

    char[][] boardData = {{'s', '-', '7', ' '}, {' ', '|', 'L', '7'}, {'-', 'F', ' ', '|'}, {'7', 'F', '-', 'J'}, {' ', 'g', ' ', '-'}};
    private MediaPlayer mp;

    public StringProperty timeLeft;
    private TimerTask timerTask;
    private IntegerProperty moves;
    private BooleanProperty completedProperty;

    @FXML
    Text countdown;
    @FXML
    Text moveCounter;

    private Timer timer;

    URL location;
    ResourceBundle resources;

    @FXML
    PipeBoard pipeBoardDisplayer;

    @FXML
    BorderPane rootPane;

    public String[][] charMatrixToString(char[][] charM){
        String[][] stringBoard = new String[boardData.length][boardData[0].length];
        for(int i=0; i<boardData.length;i++){
            for(int j=0; j<boardData[0].length;j++){
                stringBoard[i][j] = String.valueOf(boardData[i][j]);
            }
        }
        return stringBoard;
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // create solve
        this.location = location;
        this.resources = resources;
        pipeBoardDisplayer.setBoardData(charMatrixToString(boardData));
        this.moves = new SimpleIntegerProperty();
        this.completedProperty = new SimpleBooleanProperty();
        completedProperty.bind(pipeBoardDisplayer.getGameStatusProperty());
    }


    public void startTimer(){
        timer = new Timer();
        timeLeft = new SimpleStringProperty();
        timeLeft.setValue("20");
        this.completedProperty.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                // Only if completed
                if (newValue) {
                    Button w = new Button("You Won :)");
                    rootPane.setCenter(w);
                }
            }
        });
        timerTask = new TimerTask() {
            @Override
            public void run() {
                timeLeft.set(String.valueOf(Integer.valueOf(timeLeft.get())-1));
                countdown.setText("Time: " + timeLeft.get());
                moveCounter.setText(" Moves: " + pipeBoardDisplayer.getCounter().get());
                if(Integer.valueOf(timeLeft.get())  <= 0) {
                   // implement pop up window
                    //stop timer
                    Button lost = new Button("You Lost :(");
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            rootPane.setCenter(lost);
                        }
                    });
                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 1000, 1000);
    }
    public void  moveCounter(){

    }
    public void openFile() throws FileNotFoundException {
        FileChooser fc = new FileChooser();
        fc.setTitle("Open File");
        fc.setInitialDirectory(new File("./resources"));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt"));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml"));
        File chosen = fc.showOpenDialog(null);
        if (chosen != null) {
            String board;
            Scanner scanner = new Scanner(chosen);
            board = scanner.nextLine();
            while(scanner.hasNextLine()){
                board += "\n" + scanner.nextLine();
            }
            String[] lineArray = board.split("\n");
            char[][] boardData = new char[lineArray.length][lineArray[0].length()];
            for(int i=0;i<lineArray.length;i++) {
                for (int j = 0; j < lineArray[0].length(); j++) {
                    boardData[i][j] = lineArray[i].charAt(j);
                }
            }
            this.boardData = boardData;
            initialize(this.location,this.resources);
            startTimer();
        }
    }
    public void solve(){}

    public void openConfigurationWindow() {
        ConfigWindowController cwc = new ConfigWindowController(this);
        cwc.showStage();
    }
    public void save() {
        //save config if not Save level progression
        if (boardData != null) {
            FileChooser fc = new FileChooser();
            fc.setTitle("Save file");
            fc.setInitialDirectory(new File("./resources"));
            fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt"));
            fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml"));
            fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Object files (*.obj)", "*.obj"));
            File chosen = fc.showSaveDialog(null);
            if (chosen != null) {

            }
        }
    }

    public MediaPlayer getMediaPlayer() {
        return this.mp;
    }
    public void setMediaPlayer(MediaPlayer other) {
        this.mp = other;
        mp.setAutoPlay(true);
        mp.setVolume(0.1);
    }
    public String getCountdown() {
        return timeLeft.get();
    }
    public void setCountdown(String countdown) {
        this.timeLeft.set(countdown);
    }
}
